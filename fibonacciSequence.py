iteration = int(input('Enter number of iterations of the Fibonacci sequence: '))
sequence = [0, 1]

def fibonacciSequence():
    i = 0
    while i < iteration:
        new_number = sequence[-1] + sequence[-2]
        sequence.append(new_number)
        i += 1
    return new_number

fibonacciSequence()
print(sequence)


